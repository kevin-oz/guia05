/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.control;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import uesocc.edu.sv.ingenieria.prn335.entities.TipoVehiculo;

/**
 *
 * @author kevin Figueroa
 */
@Stateless
@LocalBean
public class Utilidades implements Serializable {

    @PersistenceContext(unitName = "uesocc.edu.sv.ingenieria.prn335_guia_05_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     *
     * @param entity entidad para ser creada en la base de datos
     */
    public void insert(TipoVehiculo entity) {

        em.flush();
        em.clear();

        try {
            if (entity != null) {
                em.getTransaction().begin();
                em.persist(entity);
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            em.getTransaction().rollback();
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);

        }

    }

    /**
     *
     * @param entity entidad para ser actualizada en la base de datos
     */
    public void update(TipoVehiculo entity){
        em.flush();
        em.clear();
        try {
            if (entity != null) {
                em.getTransaction().begin();
                em.merge(entity);
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            em.getTransaction().rollback();
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e.getMessage());

        }

    }

    /**
     *
     * @param entity entidad para ser eliminada en la base de datos
     */
    public void delete(TipoVehiculo entity){
        em.flush();
        em.clear();
        try {
            if (entity != null) {
                em.getTransaction().begin();
                em.remove(entity);
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            em.getTransaction().rollback();
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e.getMessage());

        }

    }

    /**
     *
     * @return todos los registros de mi base de datos
     */
    public List selectAll() {
        em.flush();
        em.clear();
        List<TipoVehiculo> lista = new ArrayList<>();

        try {
            TypedQuery<TipoVehiculo> query = em.createQuery("SELECT tp FROM TipoVehiculo tp", TipoVehiculo.class);

            lista = query.getResultList();

        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);

        }

        return lista;
    }

    /**
     *
     * @param busqueda registro a buscar en mi base de datos
     * @return lista con las coincidencias del registro buscado
     */
    public List findByCampos(String busqueda) {
        em.flush();
        em.clear();
        List<TipoVehiculo> list = new ArrayList<>();

        try {
            TypedQuery<TipoVehiculo> q = em.createQuery("SELECT tp FROM TipoVehiculo tp WHERE tp.idTipoVehiculo LIKE '" + busqueda + "' OR tp.cantidadEjes LIKE '" + busqueda + "' OR tp.pesoMaxLbs LIKE '" + busqueda
                    + "' OR tp.activo LIKE '" + busqueda + "' OR tp.pesoMinLbs LIKE '" + busqueda + "' OR tp.descripcion LIKE '" + busqueda + "' OR tp.nombre LIKE '" + busqueda + "'", TipoVehiculo.class);
            list = q.getResultList();
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);
            
        }
        return list;

    }

    /**
     *
     * @param inicio valor del rango inicial de los registros a buscar
     * @param fin valor del rango final de los registros a buscar
     * @return lista con los registros dentro del rango asignado
     */
    public List findRange(int inicio, int fin) {

        List<TipoVehiculo> list = new ArrayList<>();
        em.flush();
        em.clear();
        try {
            TypedQuery<TipoVehiculo> q = em.createQuery("SELECT tp FROM TipoVehiculo tp", TipoVehiculo.class);
            q.setFirstResult(inicio);
            q.setMaxResults(fin);
            list = q.getResultList();
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);
            
        }
        return list;
    }

    /**
     *
     * @param campos a buscar dentro de mi database
     * @param busquedas parametros a buscar dentro de los campos
     * @return lista con las coincidencias encontradas
     */
    public List findByMultiple(String[] campos, String[] busquedas) {

        em.flush();
        em.clear();
        List<TipoVehiculo> List = new ArrayList<>();

        String q = "SELECT tp FROM TipoVehiculo tp WHERE tp." + campos[0] + " = " + busquedas[0];
        if (campos.length > 1) {
            for (int i = 1; i < campos.length; i++) {
                q += " and tp." + campos[i] + " = " + busquedas[i];
            }
        }
        Query querys = em.createQuery(q, TipoVehiculo.class);
        List = querys.getResultList();

        return List;

    }

}
